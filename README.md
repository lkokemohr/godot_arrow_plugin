# README #

This is a plugin for Godot which adds a new node for arrows.

## Properties: ##
**Target Node Path**: path to a node to point at

**Target**: global position to point at

**Width**: width of the arrow in pixels

**Start Offset**: distance of the start of the arrow from the position of the arrow node

**End Offset**: distance of the end of the arrow from the target

**Side Offset**: offset to the right (useful if you want to have arrows from A to B and from B to A.

**Color**: color of the arrow

**Editor Only**: hides the arrow in the game so it's only visible in the editor